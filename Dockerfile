FROM alpine:latest

WORKDIR /root

RUN apk update && \
    apk add openjdk11 && \
    apk add maven && \
    apk add git

RUN git clone https://gitlab.com/faneleshezitiger/ewc10_brownfields.git

WORKDIR /root/ewc10_brownfields

CMD ["tail", "-f", "/dev/null"]