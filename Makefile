JAR_FILE = reference-server-0.2.3.jar
Run: RunJar Maven KillPort

RunJar: $(JAR_FILE)
	java -jar $(JAR_FILE) &

#RunServer:
#	de.wethinkco.robotworlds.Server

KillPort:
	-pkill -f "java.* 5000"
Maven:
	mvn clean
	mvn validate
	mvn compile
	mvn verify
	mvn test
	mvn package



