package de.wethinkco.robotworlds;

import de.wethinkco.robotworlds.helpers.JsonHelper;
import de.wethinkco.robotworlds.protocol.*;
import de.wethinkco.robotworlds.protocol.robots.Robot;
import de.wethinkco.robotworlds.protocol.world.ServerConfig;
import de.wethinkco.robotworlds.protocol.world.World;

import java.io.*;
import java.net.Socket;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represents a handler for a client connection in the Robot Worlds server.
 * When a client connects, the server spawns a thread for each client, allowing the server
 * to handle multiple clients simultaneously.
 * This keyword should be used in setters, passing the object as an argument,
 * and to call alternate constructors
 * a constructor with a different set of arguments.
 */

// Runnable is implemented on a class whose instances will be executed by a thread.
public class ClientHandler implements Runnable {

    // Array list of all the threads handling clients.
    public static ArrayList<ClientHandler> clientHandlers = new ArrayList<>();

    // Socket for a connection, buffer reader and writer for receiving and sending data respectively.
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    private String clientUsername;
    private String clientMake;

    // JsonHelper for serializing and deserializing JSON.
    private final JsonHelper jsonHelper = new JsonHelper();

    // Reference to the world the server is managing.
    private final World world;

    /**
     * Constructor to initialize a ClientHandler with a socket and the world it belongs to.
     * @param socket The socket for the client connection.
     * @param world The world instance managed by the server.
     */
    public ClientHandler(Socket socket, World world) {
        this.world = world;
        try {
            this.socket = socket;
            this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter= new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            // Add this client handler to the list of active handlers.
            clientHandlers.add(this)
            ;
        } catch (IOException e) {
            // Close everything more gracefully in case of an exception.
            closeEverything(socket, bufferedReader, bufferedWriter);
        }
    }

    /** Runnable method to be executed on a separate thread for listening to client messages.
     * Everything in this method is run on a separate thread. We want to listen for messages
    * on a separate thread because listening (bufferedReader.readLine()) is a blocking operation.
    * A blocking operation means the caller waits for the callee to finish its operation.
     */
    @Override
    public void run() {
        String jsonFromClient;
        // Continue to listen for messages while a connection with the client is still established.
        while (socket.isConnected()) {
            try {
                // Read what the client sent
                jsonFromClient = bufferedReader.readLine();

                // Check if the client has closed the connection
                if (jsonFromClient == null) {
                    System.out.println("Client disconnected: " + this.socket);
                    removeClientHandler();
                    break;  // Exit the loop if the client disconnected
                }
//                System.out.println(jsonFromClient);
                System.out.println("Request from " + this.socket + ": " + jsonFromClient);

                if (jsonFromClient.equals("null")) {
                    // Handle unsupported command
                    Map<String, Object> mapData = new HashMap<>();
                    mapData.put("message", "Unsupported command");
                    ResponseMessage responseMessage = new ErrorResponseMessage("ERROR", mapData);
                    HandleUnsupportedCommand(responseMessage);
                } else {
                    // Process the received JSON
                    String json = jsonHelper.addType(jsonFromClient);
                    RequestMessage requestMessage = jsonHelper.JsonToRequestMessage(json);
                    HandleRequestMessage(requestMessage);
                }
            } catch (IOException e) {
                // Close everything gracefully.
                closeEverything(socket, bufferedReader, bufferedWriter);
                break;
            }
        }
    }

    /**
     *  Method to handle a request message received from the client.
     * @param requestMessage
     * @throws IOException
     */
    private void HandleRequestMessage(RequestMessage requestMessage) throws IOException {
        ResponseMessage responseMessage = null;
        Robot robot = this.world.getRobotByName(this.clientUsername);
        System.out.println(requestMessage.getArguments());
        if (requestMessage.getRobot()==null && !(requestMessage instanceof LaunchRequestMessage)){
            Map<String, Object> mapData = new HashMap<>();
            mapData.put("message", "No robot launched");
            responseMessage = new ErrorResponseMessage("ERROR", mapData);

        } else if (!(robot == null) && robot.isDead()) {
            Map<String, Object> mapData = new HashMap<>();
            mapData.put("message", "Your robot is dead.");
            responseMessage = new ErrorResponseMessage("ERROR", mapData);
            closeEverything(socket, bufferedReader, bufferedWriter);
        }
        else {
            if (!(robot == null) && robot.isReloading()){
                robot.setStatus(RobotStatus.NORMAL);
            }
            responseMessage = requestMessage.execute(robot, this.world);

            if (requestMessage instanceof LaunchRequestMessage
                    && responseMessage instanceof SuccessResponseMessage){
                this.clientUsername = (String) requestMessage.getArguments().get(1);
            }
        }

        String jsonToSend = jsonHelper.ResponseMessageToJson(responseMessage);


        String json = jsonHelper.removeType(jsonToSend);

        try {
            this.bufferedWriter.write(json);
            this.bufferedWriter.newLine();
            this.bufferedWriter.flush();
        } catch (IOException e) {
            closeEverything(socket, bufferedReader, bufferedWriter);
        }
        // if quit was successful, then close off the connection and remove the clienthandler
        // only close off after response written to client
        if (requestMessage instanceof ShutdownQuitOffRequestMessage
                && responseMessage instanceof SuccessResponseMessage){
            closeEverything(socket,bufferedReader,bufferedWriter);
        }
    }

    /**
     * Method to handle an unsupported command and send an error response to the client.
     * @param errorResponse
     */
    private void HandleUnsupportedCommand(ResponseMessage errorResponse){
        String jsonToSend = jsonHelper.ResponseMessageToJson(errorResponse);

        try{
            this.bufferedWriter.write(jsonToSend);
            this.bufferedWriter.newLine();
            this.bufferedWriter.flush();
        } catch (IOException e){
            closeEverything(socket, bufferedReader, bufferedWriter);
        }
    }


    /**
     *  Method to remove the client handler from the list when the client disconnects.
     *  If the client disconnects for any reason remove them from the list so their not interacted with in world
     */
    public void removeClientHandler() {
        clientHandlers.remove(this);
    }

    /**
     *  Helper method to close the socket, buffer reader, and buffer writer.
      */

    public void closeEverything(Socket socket, BufferedReader bufferedReader, BufferedWriter bufferedWriter) {
        /** Note you only need to close the outer wrapper as the underlying streams are closed when you close the wrapper.
        * Note you want to close the outermost wrapper so that everything gets flushed.
        * Note that closing a socket will also close the socket's InputStream and OutputStream.
        * Closing the input stream closes the socket. You need to use shutdownInput() on socket to just close the input stream.
        * Closing the socket will also close the socket's input stream and output stream.
        * Close the socket after closing the streams.
         */

        // If the client disconnects for any reason remove them from the list so their not interacted with in world
        removeClientHandler();
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Sends the server configuration to the client.
     * @param bufferedWriter The buffered writer for writing to the client.
     */
    public void sendServerConfig(BufferedWriter bufferedWriter){
        ServerConfig serverConfig = Server.getConfig();
        String serverConfigJson = jsonHelper.ServerConfigToJson(serverConfig);

        try {
            this.bufferedWriter.write(serverConfigJson);
            this.bufferedWriter.newLine();
            this.bufferedWriter.flush();
        } catch (IOException e) {
            closeEverything(socket, bufferedReader, bufferedWriter);
        }
    }


    // Overridden toString method for better representation of the client handler.
    @Override
    public String toString() {
        return "ClientHandler{" +
                "clientUsername='" + clientUsername + '\'' +
                ", robot=" + this.world.getRobotByName(this.clientUsername) +
                '}';
    }
}