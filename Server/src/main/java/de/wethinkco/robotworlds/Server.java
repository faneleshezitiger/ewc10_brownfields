package de.wethinkco.robotworlds;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.wethinkco.robotworlds.protocol.world.*;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.PatternSyntaxException;

/**
 * The main server class responsible for handling client connections and managing the Robot Worlds game.
 * Server commands:
 * - Quit: Ends the game and closes all sockets.
 * - Robots: Displays information about how many robots are currently playing.
 * - Dump: Allows viewing everything that's in the World.
 */
public class Server implements Runnable {

    private static final int PORT = 5003;

    private final ServerSocket serverSocket;
    private World world;

    public static void setPort(int invalidPort) {

    }

    public World getWorld(){
        return world;
    }


    public static boolean ObstacleFlag =false;

    /**
     * Constructor to initialize a Server with a server socket and create a new World instance.
     * @param serverSocket The server socket for accepting client connections.
     */
    public Server(ServerSocket serverSocket, ArrayList<int[]> obstacles) {
        world = new World(obstacles);
        this.serverSocket = serverSocket;
    }

    public Server(ServerSocket serverSocket) {
        world = new World();
        this.serverSocket = serverSocket;
    }

    /**
     * Gets the port number on which the server is running.
     * @return The port number.
     */
    public static int getPort() {
        if (PORT < 1024 || PORT > 9999) {
            throw new IllegalArgumentException("Invalid default port number. Please set a number between 1024 and 9999.");
        }
        return PORT;
    }

    /**
     * Main method to start the server, configure world properties, and handle user commands.
     * @param args Command-line arguments.
     * @throws IOException If an I/O error occurs.
     */
    public static void main(String[] args) throws IOException {
        int port = getPort(); // default port
        int width = 100;
        int height = 200;
        ArrayList<int[]> coordinatesList = new ArrayList<>();
        //Might need to add a new class to handle the command line stuff.

        Options options = new Options();

        Option input = new Option("p", "port", true, "input port number");
        Option size = new Option("s", "size", true, "input world size");
        Option obstacle = new Option("o", "obstacle", true, "input world obstacle");
        input.setRequired(false);
        size.setRequired(false);
        obstacle.setRequired(false);
        options.addOption(input);
        options.addOption(size);
        options.addOption(obstacle);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
            return;
        }

        if (cmd.hasOption("port")) {
            String portString = cmd.getOptionValue("port");
            try {
                int tempPort = Integer.parseInt(portString);
                if (tempPort >= 1024 && tempPort <= 9999) {
                    port = tempPort;
                } else {
                    System.out.println("Invalid port number. Please enter a number between 1024 and 9999.");
                    return;
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid port number. Please enter a valid number.");
                return;
            }
        }
        if(cmd.hasOption("size")){
            String portString = cmd.getOptionValue("size");
            try{
                if(Integer.parseInt(portString)>0){
                    height = Integer.parseInt(portString);
                    width = Integer.parseInt(portString);
                }
            }catch(NumberFormatException e){
                System.out.println("Wrong size value.");
                return;
            }

        }

        if(cmd.hasOption("obstacle")){
            String portString = cmd.getOptionValue("obstacle");
            try {
                String[] coordinates = portString.split(",");
                coordinatesList.add(new int[]{Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1])});
                ObstacleFlag=true;


            }catch (PatternSyntaxException e){
                System.out.println("Wrong obstacle input, eg. 0,0 ");
                return;
            }
        }



        System.out.println("Robot World server running and waiting for client connections on port " + port);


        WorldProperties.changeProperties(width,height,10);
        getConfig();

        Scanner scanner = new Scanner(System.in);
        promptChange(scanner);
        Server server;
        ServerSocket serverSocket = new ServerSocket(port);
        if(ObstacleFlag){
            server = new Server(serverSocket, coordinatesList);
        }else{
            server = new Server(serverSocket);
        }

        Thread serverThread = new Thread(server);
        serverThread.start();
        // Set Config file to default settings.
//        System.out.println(width+"---"+height);
        while(true){
            System.out.println("Enter your command:");
            String command = scanner.nextLine();
            //handle command here
            if(command.equalsIgnoreCase("robots")){
                for (ClientHandler client :
                        ClientHandler.clientHandlers) {
                    System.out.println(client.toString());
                }
            }
//            System.out.println("Port: " + port);
            if(command.equalsIgnoreCase("dump")){
                System.out.println(server.getWorld().toString());
            }
            if(command.equalsIgnoreCase("quit")){
                break;
            }
            if(command.equalsIgnoreCase("help")){
                System.out.println("\n" + ServerHelp());
            }

        }
        System.out.println("Shutting down server...");
        server.closeServerSocket();

    }


    /**
     * Prompts the user to change world properties and updates the configuration accordingly.
     * @param input The Scanner object for user input.
     * @throws IOException If an I/O error occurs.
     */
    private static void promptChange(Scanner input) throws IOException {
        WorldProperties.showProperties();
        while(true){
            System.out.println("Would you like to change world properties? Y/N");
            String answer = input.nextLine();

            if(answer.equalsIgnoreCase("y")||answer.equalsIgnoreCase("yes")) {
                WorldProperties.changeProperties(WorldProperties.getWidth(),WorldProperties.getHeight(),WorldProperties.getVisibility());
                System.out.println("World Properties successfully changed.");
                getConfig();
                break;
            } else if(answer.equalsIgnoreCase("n")||answer.equalsIgnoreCase("no")){
                break;
            }
        }

    }

    /**
     * Displays a help message containing information about available server commands.
     * @return The help message.
     */
    public static String ServerHelp(){
        //Temporary even//
        return "What you can do here... \n\n " +
                "Dump: To see the world and everything in it.\n " +
                "Robots: To list robots currently in the world. Including their states.\n " +
                "Quit: To shut down the server\n ";
    }


    /**
     * Starts the server and listens for client connections on the specified port.
     * Creates a new client handler thread for each connected client.
     */
    public void startServer() {
        try {
//            List<IObstacle> list = new ArrayList<IObstacle>();

            // Listen for connections (clients to connect) on port 1234.
//            IObstacle obstacle = new SquareObstacle(1,1);
//            list.add(obstacle);

            while (!serverSocket.isClosed()) {
                // Will be closed in the RobotClient Handler.
                Socket socket = serverSocket.accept();
                System.out.println(GridSize.getHeight()+"--"+GridSize.getWidth());
                System.out.println(new ClientConfig().getGridHeight()+"--"+new ClientConfig());
                System.out.println("A new client has connected!");
                ClientHandler clientHandler = new ClientHandler(socket, world);
                Thread thread = new Thread(clientHandler);
                // The start method begins the execution of a thread.
                // When you call start() the run method is called.
                // The operating system schedules the threads.
                thread.start();
            }
        } catch (IOException e) {
            closeServerSocket();
        }
    }


    /**
     * Closes the server socket gracefully.
     */
    public void closeServerSocket() {
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Runnable method to be executed on a separate thread.
     */
    @Override
    public void run() {
        startServer();
    }


    /**
     * Reads the server configuration from the Configuration.json file.
     * @return The ServerConfig object representing the server configuration.
     */
    public static ServerConfig getConfig() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
//            String projectRootPath = Paths.get("").toAbsolutePath().toString();
            String projectRootPath = System.getProperty("user.dir");
            String configFilePath = projectRootPath.concat("/Configuration.json");
            ServerConfig serverConfig = mapper.readValue(Paths.get(configFilePath).toFile(), ServerConfig.class);
            return serverConfig;
        } catch (Exception ex) {
            System.out.println("Failed to read from Server Config: " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }
}