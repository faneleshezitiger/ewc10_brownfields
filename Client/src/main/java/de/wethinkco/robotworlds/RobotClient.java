/**
 * The RobotClient class represents a client that connects to the Robot Worlds server.
 * It allows users to interact with the server by sending messages and receiving responses.
 * The client communicates with the server through a socket connection and maintains a state
 * representing the user's robot in the game.
 */
package de.wethinkco.robotworlds;
import de.wethinkco.robotworlds.protocol.*;
import de.wethinkco.robotworlds.protocol.robots.Robot;
import de.wethinkco.robotworlds.helpers.JsonHelper;
import de.wethinkco.robotworlds.protocol.world.ServerConfig;
import java.io.*;
import java.net.*;
import java.util.*;
import static de.wethinkco.robotworlds.InputValidation.checkIpAndPort;

/**
 * The RobotClient class allows users to connect to the Robot Worlds server, send commands,
 * and receive responses. It also maintains the state of the user's robot in the game.
 */
public class RobotClient {
    // Indicates whether the user's robot has been launched.
    public boolean launch = false;

    // Socket, readers, and writers for communication with the server.
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;

    // Additional readers and writers for input and output.
    private BufferedReader in;
    private BufferedWriter out;

    // User's username and JSON helper for serialization/deserialization.
    private String username;
    private final JsonHelper jsonHelper = new JsonHelper();

    // State representing the user's robot.
    private Robot state;

    // Indicates whether the user has requested help.
    public boolean helpRequest = false;


    public Robot getState(){
        return this.state;
    }


    /**
     * Default constructor for RobotClient.
     */
    public RobotClient(){

    }


    /**
     * Constructor for RobotClient with a pre-established socket connection.
     *
     * @param socket The socket representing the connection to the server.
     */
    public RobotClient(Socket socket) {
        try {
            this.socket = socket;
            this.username = "";
            this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter= new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
//            doServer();
//            String serverConfigJson = bufferedReader.readLine();
//            ServerConfig serverConfig = jsonHelper.JsonToServerConfig(serverConfigJson);

        } catch (IOException e) {
            // Gracefully close everything.
            closeEverything(socket, bufferedReader, bufferedWriter);
        }
    }

    /**
     * Sends a message to the server using a separate thread.
     */
    public void sendMessage() {
        new Thread(() -> {
            try {
//            // Initially send the username of the client.
//            bufferedWriter.write(username + " " + make);
//            bufferedWriter.newLine();
//            bufferedWriter.flush();
                // Create a scanner for user input.
                Scanner scanner = new Scanner(System.in);
                String commandString = null;
                // While there is still a connection with the server, continue to scan the terminal and then send the message.

                System.out.print("Welcome to Robots! \n");

                while (socket.isConnected()) {

                    if (!launch){
                        System.out.println("Make sure to launch your robot before you do anything else!"
                                + " (use format 'launch (make) (robot_name)'");
                        //Robots
                    }

                    System.out.print("Enter Command: ");
                    commandString = scanner.nextLine().toLowerCase();
                    if(commandString.equalsIgnoreCase("help")){
                        System.out.println("\n" + ClientHelp());
                        continue;
                    }


                    if(new ArrayList<>(Arrays.asList(new String[]{"quit", "off", "shutdown","close"})).contains(commandString.toLowerCase())){
                        System.out.println("Shutting down...");
                        closeEverything(socket, bufferedReader, bufferedWriter);
                        System.out.println("Thank you for playing Robot World!");
                        System.exit(0);
                    }

                    RequestMessage requestMessage = HandleCommand(commandString);

                    //Checks if username is blank and requestMessage is a launch message
                    // Then assigns username if conditions meet
                    try {
                        checkUsername(requestMessage);
                    }catch (IndexOutOfBoundsException e){
                        System.out.println("Index Out of bound!");
                        continue;
                    }


                    sendRequest(commandString);
                    listenForMessage();
                }
            } catch (IOException e) {
                // Gracefully close everything.

                closeEverything(socket, bufferedReader, bufferedWriter);
            }
        }).start();
    }

    /**
     * Listens for messages from the server on a separate thread.
     */
    public void listenForMessage() {

        String jsonResponseFromServer;
        // While there is still a connection with the server, continue to listen for messages on a separate thread.
        if (socket.isConnected()) {
            try {
                // Get the json response sent from the server.
                ResponseMessage message = getResponse();


                if(message.getResult().equalsIgnoreCase("ok") &&
                        ((SuccessResponseMessage)message).getState() != null){
                    this.state = ((SuccessResponseMessage)message).getState();
                }
                handleResponse(message);
            } catch (IOException e) {
                // Close everything gracefully.
                closeEverything(socket, bufferedReader, bufferedWriter);
            }
        } else {
            throw new RuntimeException("You are no longer connected to the server.");
        }
    }


    /**
     * Handles the user command and creates a corresponding request message.
     *
     * @param commandString The user's command string.
     * @return The generated RequestMessage object.
     */
    private RequestMessage HandleCommand(String commandString) {

        return RequestMessage.createRequest(this.username, commandString);
    }


    /**
     * Prints a help message for the client.
     *
     * @return A string containing the help information.
     */
    public static String ClientHelp(){
        //Temporary//
        return "Here is the little help to get you through this game!\n\n " +
                "Forward/Back: To move forward/backward use type the movement, and specify the number of steps(argument) you want move. (e.g forward 5)\n " +
                "Right/Left: For even better navigation, you can turn left or right by typing (turn right/left)\n " +
                "Fire: To shoot, type fire robots in range will take a hit\n " +
                "Reload/Repair: When you ran out of shots, use reload. if you took too much heat use repair\n " +
                "Quit: To leave the game";

    }


    /**
     * Handles the response received from the server and performs corresponding actions.
     *
     * @param responseMessage The ResponseMessage received from the server.
     */
    private void handleResponse(ResponseMessage responseMessage){


        if (responseMessage.getData().containsKey("message")){
            switch( (String) responseMessage.getData().get("message")){
                case ("failed to launch"):
                    System.out.println("Failed to launch, Try again");
                    break;
                case ("Invalid make provided"):
                    this.username = "";
                    this.launch = false;
                    System.out.println("Invalid make provided. Please make sure your formatting is correct.");
                    break;

                case ("Robot name already in use"):
                    this.username = "";
                    this.launch = false;
                    System.out.println("Another player has launched a robot with the same name.");
                    System.out.println("Please launch with a different name!");
                    break;

                case ("Robot already launched"):
                    System.out.println("You are only allowed a maximum of 1 robot at a time!");
                    break;

                case "shutdown":
                case  "off":
                case  "quit":
                    System.out.println("Shutting down...");
                    closeEverything(socket, bufferedReader, bufferedWriter);
                    System.out.println("Thank you for playing Robot World!");
                    System.exit(0);
                    break;

                case "state":
                    printState();

                case "No robot launched":
                    System.out.println("\nInvalid launch command\n");
                    System.out.print("\nE.G launch spy Hal\n");
                    break;
                case "Reloaded":
                    doReload();
                    return;
                case "Repaired":
                    doRepair();
                    return;
                case "Turned left":
                    System.out.println("\nTurned left\n");
                    break;
                case "Turned right":
                    System.out.println("\nTurned right\n");
                    break;
                case "Your robot is dead.":
                    System.out.println("\\033[H\\033[2J");
                    System.out.println("\\u001B[1m You died!");
                    System.out.println("Thank you for playing Robot Worlds!");
                    System.out.println("If you'd like to play again, relaunch the program :)");
                    break;
                case "0 steps":
                    System.out.println("No movement done, 0 steps provided.");
                    break;
                case "Not enough arguments provided.":
                    System.out.println("Not enough arguments provided");
                    break;
                case "Too many arguments provided.":
                    System.out.println("Too many arguments provided.");
                    break;
                case "There is an obstacle in the way.":
                    System.out.println("Could not move the whole way, there is a obstacle in the way.");
                    break;
                case "There is a robot in the way.":
                    System.out.println("Could not move the whole way, there is a robot in the way.");
                    System.out.println(".\n.\n.\n");
                    System.out.println("WATCH OUT!");
                    break;
                case "Sorry, I cannot go outside my safe zone.":
                    System.out.println("Sorry, I cannot move outside of my safe zone");
                    System.out.println("We've got limits too you know...");
                    break;
                case "Invalid Steps":
                    System.out.println("Could not do any movement.");
                    System.out.println("Please make sure you use a valid number...");
                    break;
                case "invalid command provided":
                    System.out.println("Invalid command provided!");
                    break;
            }

            // Print additional messages for movement commands.
            if (((String) responseMessage.getData().get("message")).toLowerCase().contains("moved forward")){
                System.out.println(((String) responseMessage.getData().get("message")));
            } else if (((String) responseMessage.getData().get("message")).toLowerCase().contains("moved back")) {
                System.out.println(((String) responseMessage.getData().get("message")));
            }

        } else if (responseMessage.getData().containsKey("position") &&
                responseMessage.getData().containsKey("shield") &&
                responseMessage.getData().containsKey("visibility")){

            // Handle responses containing robot state information.
            Map<String, Object> responseData = responseMessage.getData();
            System.out.println("\nRobot launched! " +

                    "\n  Position: " + responseData.get("position") +
                    "\n  Direction: " + responseData.get("direction") +
                    "\n  Shots: " + responseData.get("shots") +
                    "\n  Shields: " + responseData.get("shield") +
                    "\n  Visibility: " + responseData.get("visibility") +
                    "\n  Position: " + responseData.get("position"));
            return;
        }

        // Handle responses containing the result of the "look" command.
        if (responseMessage.getData().containsKey("objects")){
            List<LinkedHashMap> kiki = (List<LinkedHashMap>) responseMessage.getData().get("objects");
            List<ObstacleResponse> lookList = (List<ObstacleResponse>) responseMessage.getData().get("objects");
            if(!lookList.isEmpty()){
                System.out.println("Robot look result: ");
                for (LinkedHashMap obstacle: kiki) {
                    System.out.println("\n  Direction: " + obstacle.get("direction")+
                            "\n  Distance: " + obstacle.get("steps") +
                            "\n  Type: " + obstacle.get("typeOfObject"));
                }
            }else {
                System.out.println("Nothing around in visibility constraint");
            }
        }

        // Print the current state if the response contains a robot state.
        if (responseMessage instanceof SuccessResponseMessage &&
                ((SuccessResponseMessage)responseMessage).getState() != null){
            System.out.println();
            printState();
        }
    }


    /**
     * Prints the current state of the user's robot.
     */
    private void printState() {

        System.out.println("This is the Robot's current state: ");
        System.out.println("Position: " + this.state.getPosition());
        System.out.println("Direction: " + this.state.getDirection());
        System.out.println("Shields: " + this.state.getShields());
        System.out.println("Shots: " + this.state.getShots());
        System.out.println("Status: " + this.state.getStatus());
        System.out.println();
    }


    /**
     * Placeholder method for printing launch information.
     */
    public void printLaunch(){

    }

    /**
     * Placeholder method for repairing the robot.
     */
    private void repair(){

    }


    /**
     * Helper method to close input and output streams and the socket connection.
     *
     * @param socket         The socket connection to be closed.
     * @param bufferedReader The BufferedReader to be closed.
     * @param bufferedWriter The BufferedWriter to be closed.
     */
    public void closeEverything(Socket socket, BufferedReader bufferedReader, BufferedWriter bufferedWriter) {
        /** Note you only need to close the outer wrapper as the underlying streams are closed when you close the wrapper.
        * Note you want to close the outermost wrapper so that everything gets flushed.
        * Note that closing a socket will also close the socket's InputStream and OutputStream.
        * Closing the input stream closes the socket. You need to use shutdownInput() on socket to just close the input stream.
        * Closing the socket will also close the socket's input stream and output stream.
        * Close the socket after closing the streams.
         */
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Performs a reload animation and prints the available shots after reloading.
     */
    public void doReload(){
        System.out.println("RELOADING!");

        try
        {
            for (int i = 0; i < 7; i++) {
                System.out.print(".");
                Thread.sleep(500);
            }
        }

        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
        System.out.println("\nDONE RELOADING!");
        System.out.println("Available Shots: " + this.state.getShots());
    }


    /**
     * Performs a repair animation and prints the available shields after repairing.
     */
    public void doRepair(){
        System.out.println("REPAIRING!");

        try
        {
            for (int i = 0; i < 7; i++) {
                System.out.print(".");
                Thread.sleep(500);
            }
        }

        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
        System.out.println("\nDONE REPAIRING!");
        System.out.println("Available Shields: " + this.state.getShields());
    }

    /**
     * Checks the username based on the launch message received.
     * If the username is null or empty and the message is a LaunchRequestMessage,
     * it sets the username from the launch message.
     * @param launchMessage The launch message received.
     */
    public void checkUsername(RequestMessage launchMessage){

        if (this.username == null || this.username.isEmpty() && launchMessage instanceof LaunchRequestMessage){
            this.username = (String) launchMessage.getArguments().get(1);
            this.launch = true;
        }
    }

    /**
     * Main method to run the program.
     * Gets user input for IP and port, establishes a connection to the server,
     * and enters an infinite loop to read and send messages.
     * @param args Command-line arguments.
     * @throws IOException If an I/O error occurs.
     */
    public static void main(String[] args) throws IOException {

        // Get a username for the user and a socket connection.
        // Get a username for the user and a socket connection.
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter IP: ");
        String ip = scanner.nextLine().trim();
        System.out.print("Enter port: ");
        String port = scanner.nextLine().trim();

        while (!checkIpAndPort(ip, port)) {
            System.out.println("Invalid IP or port. Please try again.");
            System.out.print("Enter IP: ");
            ip = scanner.nextLine().trim();
            System.out.print("Enter port: ");
            port = scanner.nextLine().trim();
        }

        // Create a socket to connect to the server.
        Socket socket = new Socket();
        SocketAddress socketAddress = new InetSocketAddress(ip, Integer.parseInt(port));

        try {
            socket.connect(socketAddress, 1);
        } catch (SocketTimeoutException e) {
            System.out.println("Invalid IP, please contact Server host for IP and Port");
            RobotClient.main(new String[]{});
        }

        // Pass the socket and give the robotClient a username.
        RobotClient robotClient = new RobotClient(socket);

        // Infinite loop to read and send messages from the server.
        robotClient.sendMessage();


    }


    /**
     * Receives a response from the server and converts it to a ResponseMessage.
     * @return The ResponseMessage received from the server.
     * @throws IOException If an I/O error occurs.
     */
    public ResponseMessage getResponse() throws IOException {

        String jsonResponseFromServer = this.bufferedReader.readLine();
        String json = jsonHelper.addTypeOnClient(jsonResponseFromServer);
        String jsons = jsonHelper.update(json);

        return jsonHelper.JsonToResponseMessage(jsons);
    }


    /**
     * Sends a request message to the server.
     * @param requestMessage The request message to be sent.
     * @throws IOException If an I/O error occurs.
     */
    public void sendRequest(String requestMessage) throws IOException {
        if (requestMessage.startsWith("help")) {
            this.helpRequest = true;
        }

        RequestMessage request = RequestMessage.createRequest(this.username, requestMessage);
        checkUsername(request);

//        System.out.println();
        String jsonToSend = jsonHelper.RequestMessageToJson(request);
        String send = jsonHelper.removeType(jsonToSend);
        bufferedWriter.write(send);
        bufferedWriter.newLine();
        bufferedWriter.flush();

    }


    /**
     * Connects to the server using the specified IP address and port.
     * @param ipAddress The IP address of the server.
     * @param port The port number of the server.
     */
    public void connect(String ipAddress, String port){
        try {
            this.socket = new Socket(ipAddress, Integer.parseInt(port));
            this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter= new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            //error connecting should just throw Runtime error and fail test
            throw new RuntimeException("Error connecting to Robot Worlds server.", e);
        }
    }


    /**
     * Checks if the client is connected to the server.
     * @return True if the client is connected, false otherwise.
     */
    public boolean isConnected(){
        return this.socket.isConnected();
    }


    /**
     * Retrieves the server configuration and initializes the client accordingly.
     * @throws IOException If an I/O error occurs.
     */
    public void doServer() throws IOException {
        String serverConfigJson = bufferedReader.readLine();
        ServerConfig serverConfig = jsonHelper.JsonToServerConfig(serverConfigJson);
    }

    /**
     * Disconnects the client from the server by closing streams and the socket.
     */
    public void disconnect() {
        try {
            this.bufferedReader.close();
            this.bufferedWriter.close();
            socket.close();
        } catch (IOException e) {
            //error connecting should just throw Runtime error and fail test
            throw new RuntimeException("Error disconnecting from Robot Worlds server.", e);
        }

}

}