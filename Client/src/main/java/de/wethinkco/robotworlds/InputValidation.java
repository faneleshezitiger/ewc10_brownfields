/**
 * The InputValidation class provides methods for validating user input related to server connection details
 * and outputs information about different robot types.
 */
package de.wethinkco.robotworlds;

/**
 * The InputValidation class contains methods for checking the validity of IP addresses and port numbers,
 * and for outputting information about different robot types.
 */
public class InputValidation {

    public static boolean checkIpAndPort(String ip, String port) {
        // Check if IP address is valid.
        if (!ip.equalsIgnoreCase("localhost")) {
            if (!ip.matches("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$")) {
                System.out.println("Invalid IP format. Correct example: 192.168.0.1. " +
                        "Ask the host for the correct IP.");
                return false;
            }

            String[] splitIP = ip.split("\\.");
            for (String s : splitIP) {
                int ipPart;
                try {
                    ipPart = Integer.parseInt(s);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid IP format. Correct example: 192.168.0.1. " +
                            "Ask the host for the correct IP.");
                    return false;
                }

                if (ipPart < 0 || ipPart > 255) {
                    System.out.println("Invalid IP format. Correct example: 192.168.0.1. " +
                            "Ask the host for the correct IP.");
                    return false;
                }
            }
        }

        // Check if port number is valid.
        int portNumber;
        try {
            portNumber = Integer.parseInt(port);
        } catch (NumberFormatException e) {
            System.out.println("Invalid port number format. Correct example: 5050. Ask the host for the correct port.");
            return false;
        }

        if (portNumber < 1024 || portNumber > 9999) {
            System.out.println("Invalid port number. Port number should be between 1024 and 65535. Ask the host for the correct port.");
            return false;
        }

        // If all checks pass, return true
        return true;
    }

    /**
     * Outputs information about different robot types.
     */
    public static void outputMakes() {
        System.out.println("Spy - Close ranged robot that is allowed " +
                "to take up to 5 shots all dealing 1 damage each.");
        System.out.println("Runner - Another close ranged robot that is allowed to take up " +
                "to 4 shots before reloading, all dealing 2 damage each.");
        System.out.println("Assault - A medium ranged, all-rounder robot that is allowed " +
                "to take up to 3 shots, all dealing 3 damage each.");
        System.out.println("Heavy - A medium-close ranged robot that is allowed " +
                "to take up to 2 shots, both dealing 4 damage each.");
        System.out.println("Sniper - A long ranged robot that is only allowed " +
                "to take one shot, dealing 5 damage. " +
                "If you choose this robot, it is advised to keep your distance, since you will have to reload often.");
        System.out.println();
    }
}
