package de.wethinkco.robotworlds;


import de.wethinkco.robotworlds.protocol.robots.Robot;
import de.wethinkco.robotworlds.protocol.world.World;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import de.wethinkco.robotworlds.RobotClient;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class ClientTest {
    @Test
    public void TestHelp(){
        String expected = "Here is a little help to get you through the game!\n\n " +
                "Forward/Back: To move forward/backward use type the movement, and specify the number of steps(argument) you want move. (e.g forward 5)\n " +
                "Right/Left: For even better navigation, you can turn left or right by typing (turn right/left)\n " +
                "Fire: To shoot, type fire robots in range will take a hit\n " +
                "Reload/Repair: When you ran out of shots, use reload. if you took too much heat use repair\n " +
                "Quit: To leave the game";


        Assertions.assertEquals(expected, RobotClient.ClientHelp());

    }

}