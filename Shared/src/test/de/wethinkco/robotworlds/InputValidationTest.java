package de.wethinkco.robotworlds;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class InputValidationTest {

    @Test
    public void testCheckIpAndPort() {
        // Test with valid IP and port
        assertTrue(InputValidation.checkIpAndPort("127.0.0.1", "5000"));
        assertTrue(InputValidation.checkIpAndPort("localhost", "5000"));

        // Test with invalid IP
        assertFalse(InputValidation.checkIpAndPort("300.0.0.1", "5000")); // IP address out of range
        assertFalse(InputValidation.checkIpAndPort("invalid", "5000")); // Not an IP address

        // Test with invalid port
        assertFalse(InputValidation.checkIpAndPort("127.0.0.1", "70000")); // Port number out of range
        assertFalse(InputValidation.checkIpAndPort("127.0.0.1", "invalid")); // Not a number
    }
}