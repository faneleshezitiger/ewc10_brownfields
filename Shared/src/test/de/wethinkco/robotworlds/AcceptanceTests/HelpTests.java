package de.wethinkco.robotworlds.AcceptanceTests;

import de.wethinkco.robotworlds.RobotClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class HelpTests {
    private final RobotClient robotClient = new RobotClient();

    @BeforeEach
    void connectClient() throws IOException {
        robotClient.connect("localhost", "5000");
        robotClient.sendRequest("launch runner HAL");
    }

    @AfterEach
    void disconnectClient(){
        robotClient.disconnect();
    }

    @Test
    void validHelpCommandShouldSucceed() throws IOException {
        assertTrue(robotClient.isConnected());
        String request = "help";
        robotClient.sendRequest(request);
        // Since the help command doesn't interact with the server, we don't expect a response
        // Instead, we could check if the helpRequest flag is set to true
        assertTrue(robotClient.helpRequest);
        // We can also check if the help message is printed correctly
        String expectedHelpMessage = "Here is the little help to get you through this game!\n\n " +
                "Forward/Back: To move forward/backward use type the movement, and specify the number of steps(argument) you want move. (e.g forward 5)\n " +
                "Right/Left: For even better navigation, you can turn left or right by typing (turn right/left)\n " +
                "Fire: To shoot, type fire robots in range will take a hit\n " +
                "Reload/Repair: When you ran out of shots, use reload. if you took too much heat use repair\n " +
                "Quit: To leave the game";
        assertEquals(expectedHelpMessage, RobotClient.ClientHelp());
    }

    @Test
    void helpCommandWithArgumentsShouldFail() throws IOException {
        assertTrue(robotClient.isConnected());
        String request = "help arg"; // "help" command with additional arguments
        robotClient.sendRequest(request);
        // Check if the helpRequest flag is not set to true
        assertTrue(robotClient.helpRequest);
    }

    @Test
    void helpCommandWhenNotConnectedShouldFail() {
        robotClient.disconnect(); // Disconnect the client
        String request = "help";
        Exception exception = assertThrows(IOException.class, () -> {
            robotClient.sendRequest(request);
        });

        String expectedMessage = "Stream closed";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}