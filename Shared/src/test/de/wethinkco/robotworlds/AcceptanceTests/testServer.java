package de.wethinkco.robotworlds.AcceptanceTests;

import de.wethinkco.robotworlds.RobotClient;
import de.wethinkco.robotworlds.Server;
import de.wethinkco.robotworlds.helpers.JsonHelper;
import de.wethinkco.robotworlds.protocol.RequestMessage;
import de.wethinkco.robotworlds.protocol.ResponseMessage;
import de.wethinkco.robotworlds.protocol.SuccessResponseMessage;
import org.junit.After;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.*;

import static org.junit.jupiter.api.Assertions.*;

public class testServer {
//    private final ServerSocket serverSocket = new ServerSocket(5000);
//    private final Server server = new Server(serverSocket);
    private final RobotClient robotClient = new RobotClient();
    private final JsonHelper json = new JsonHelper();
    @BeforeEach
    void connectClient() throws IOException {
        robotClient.connect("localhost", "5000");
//        robotClient.doServer();
    }



    @AfterEach
    void discountClient(){
        robotClient.disconnect();
//        server.closeServerSocket();
    }
    @Test
    void validLaunchShouldSucceed() throws IOException {

        assertTrue(robotClient.isConnected());
        String request = "launch runner bo";
        robotClient.sendRequest(request);
        robotClient.sendRequest("forward 10");
        ResponseMessage responsesMessage = robotClient.getResponse();

    }
    public testServer() throws IOException {
    }
}
