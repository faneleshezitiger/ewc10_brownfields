package de.wethinkco.robotworlds.AcceptanceTests;

/*Story: Customizing Server Port
As a server administrator,
I want to customize the port my server listens on,
So that I can control the network traffic and security of my server.
        Scenario 1: Valid port number should be accepted
Given that I am running the Robot Worlds server,
And the server is asking me if I want to set a custom port number,
When I respond with "yes" and provide a valid port number in the format -p 2000,
Then the server should accept the port number,
And it should start listening on the new port number.
Scenario 2: Invalid port number should be rejected
Given that I am running the Robot Worlds server,
And the server is asking me if I want to set a custom port number,
When I respond with "yes" but provide an invalid port number or format,
Then the server should reject the input,
And it should ask me to provide a valid port number again.
Scenario 3: Default port number should be used if no custom port is set
Given that I am running the Robot Worlds server,
And the server is asking me if I want to set a custom port number,
When I respond with "no",
Then the server should use the default port number,
And it should start listening on the default port number.*/

import de.wethinkco.robotworlds.Server;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.ServerSocket;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class PortCommandLineArgumentTests {

    @Test
    public void validPortNumberShouldBeAccepted() throws IOException {
        // Given
        ServerSocket serverSocket = new ServerSocket(2000);
        Server server = new Server(serverSocket);

        // When
        int port = Server.getPort();

        // Then
        assertEquals(5003, port);
    }

    @Test
    public void invalidPortNumberShouldBeRejected() {
        // Given
        int invalidPort = 1000; // Port number less than 1024
        Server.setPort(invalidPort);

        try {
            // When
            Server.getPort();
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            // Then
            assertEquals("Invalid default port number. Please set a number between 1024 and 9999.", e.getMessage());
        }
    }

    @Test
    public void defaultPortNumberShouldBeUsedIfNoCustomPortIsSet() throws IOException {
        // Given
        ServerSocket serverSocket = new ServerSocket(5003); // Default port number
        Server server = new Server(serverSocket);

        // When
        int port = Server.getPort();

        // Then
        assertEquals(5003, port);
    }
}
