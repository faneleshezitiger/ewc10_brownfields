package de.wethinkco.robotworlds.AcceptanceTests;

import de.wethinkco.robotworlds.RobotClient;
import de.wethinkco.robotworlds.helpers.JsonHelper;
import de.wethinkco.robotworlds.protocol.ResponseMessage;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TestMovement {
    //    private final ServerSocket serverSocket = new ServerSocket(5000);
//    private final Server server = new Server(serverSocket);
    private final RobotClient robotClient = new RobotClient();
    private final JsonHelper json = new JsonHelper();


    @BeforeEach
    void connectClient() throws IOException {
        robotClient.connect("localhost", "5003");
//        robotClient.doServer();
    }

    @AfterEach
    void valaYonkinto(){
        robotClient.disconnect();
//        server.closeServerSocket();
    }
    @Test
    void validForward10shouldSucceed() throws IOException {

        assertTrue(robotClient.isConnected());
        String request = "launch runner HAL";
        robotClient.sendRequest(request);
        ResponseMessage responseMessage = robotClient.getResponse();
        assertEquals(responseMessage.getResult(),"OK");
        robotClient.sendRequest("forward 10");
        ResponseMessage responsesMessage = robotClient.getResponse();

        assertEquals(responseMessage.getResult(),"OK");
    }

    @Test
    void invalidForwd10shouldFail() throws IOException {

        assertTrue(robotClient.isConnected());

        String request = "launch runner HALE";
        robotClient.sendRequest(request);
        ResponseMessage responseMessage = robotClient.getResponse();
        assertEquals("OK",responseMessage.getResult());
        robotClient.sendRequest("forwd 10");
        ResponseMessage responsesMessage = robotClient.getResponse();
        assertEquals("ERROR",responsesMessage.getResult());
    }

    @Test
    void invalidTurnCommandShouldFail() throws IOException{
        assertTrue(robotClient.isConnected());
        robotClient.sendRequest("launch spy lolo");
        robotClient.sendRequest("turn");
        ResponseMessage response = robotClient.getResponse();
        assertNotNull(response.getResult());
        assertEquals("OK",response.getResult());
    }

    @Test
    void validTurnCommandShouldSucceed() throws IOException{
        assertTrue(robotClient.isConnected());
        robotClient.sendRequest("launch runner Aso");
        robotClient.sendRequest("turn right");
        ResponseMessage response = robotClient.getResponse();
        System.out.println(json.ResponseMessageToJson(response));
        assertNotNull(response.getResult());
        assertEquals("OK",response.getResult());
    }


}
