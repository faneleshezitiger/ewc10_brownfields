package de.wethinkco.robotworlds.AcceptanceTests;

import de.wethinkco.robotworlds.RobotClient;
import de.wethinkco.robotworlds.protocol.ResponseMessage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RepairReloadTests {
    private final RobotClient robotClient = new RobotClient();
    //   before each test connect with the client
    @BeforeEach
    void connectClient() throws IOException{
        robotClient.connect("localhost", "5000");
    }
    //    after testing close everything
    @AfterEach
    void shutDown(){
        robotClient.disconnect();

    }

    @Test
    public void invalidReload() throws IOException{
        String request = "launch runner boo";
        robotClient.sendRequest(request);
        ResponseMessage responseMessage = robotClient.getResponse();
        //        fire at least once
        robotClient.sendRequest("fire");
        ResponseMessage responseMessage1 = robotClient.getResponse();
//        reload but fail the spelling
        robotClient.sendRequest("relood");
        ResponseMessage responseMessage2 = robotClient.getResponse();
        System.out.println(responseMessage2.getResult());
//        results should be error
        Assertions.assertEquals( "error",responseMessage.getResult().toLowerCase());



    }

    @Test
    public void validReload() throws IOException{
        String request = "launch runner boo";
        robotClient.sendRequest(request);
        ResponseMessage responseMessage0 = robotClient.getResponse();
        //        fire at least once
        robotClient.sendRequest("fire");
        ResponseMessage responseMessage1 = robotClient.getResponse();


        robotClient.sendRequest("reload");
        ResponseMessage responseMessage = robotClient.getResponse();
        Assertions.assertEquals( "ok",responseMessage.getResult().toLowerCase());
        System.out.println(responseMessage);

    }


//    TODO
//    repair




}
