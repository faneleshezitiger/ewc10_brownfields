package de.wethinkco.robotworlds.protocol;

import de.wethinkco.robotworlds.protocol.robots.Robot;
import de.wethinkco.robotworlds.protocol.world.*;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FireRequestMessage extends RequestMessage {
    public FireRequestMessage() {
    }

    public FireRequestMessage(String robot) {
        super(robot, "fire", new ArrayList<>());
    }

    @Override
    public ResponseMessage execute(Robot target, World world) {
        Map<String, Object> mapData = new HashMap<>();
        int distanceToObject = 0;

        if (target.getShots() > 0) {
            target.minusShots();
            System.out.println("Remaining shots: " + target.getShots());

            List<Integer> intPosition = target.getPosition();
            Position robotPosition = new Position(intPosition.get(0), intPosition.get(1));
            List<IWorldObject> obstacles = world.obstaclesInAllDirection(robotPosition, target);

            obstacles = hitObstaclesInPath(target, robotPosition, obstacles);

            if (!obstacles.isEmpty()) {
                IWorldObject hitObject = getClosestObject(target, obstacles);
                distanceToObject = getDistanceToObject(target, hitObject);

                if (hitObject.getTypeOfObject() == TypeOfObject.ROBOT) {
                    ((Robot) hitObject).applyDamage(target.getDamagePerShot());
                    mapData.put("message", "Hit");
                    mapData.put("distance", distanceToObject);
                    mapData.put("robot", ((Robot) hitObject).getRobotName());
                    mapData.put("state", hitObject);

                    // Logging the hit robot
                    System.out.println("Hit robot: " + ((Robot) hitObject).getRobotName());
                } else {
                    mapData.put("message", "Hit obstacle");
                    mapData.put("distance", distanceToObject);
                    mapData.put("obstacle", true);

                    // Logging the hit obstacle
                    System.out.println("Hit obstacle");
                }
            } else {
                mapData.put("message", "Miss");

                // Logging the miss
                System.out.println("Missed the target");
            }
        } else {
            mapData.put("message", "No ammo");

            // Logging no ammo
            System.out.println("No ammo remaining");
        }

        return new SuccessResponseMessage("OK", mapData, target);
    }
    // Method to filter obstacles that are hit by the shot
    public List<IWorldObject> hitObstaclesInPath(Robot target, Position robotPosition, List<IWorldObject> obstacles){
        List<IWorldObject> obstaclesInFire = new ArrayList<>();
        // Calculate the position at the maximum firing range
        Position rangePosition = getMaxRangePosition(target.getDirection(), robotPosition, target.getDamagePerShot());

        // Iterate through obstacles to check if they are hit
        for (IWorldObject obstacle: obstacles) {
            if (obstacle instanceof Robot){
                // If the obstacle is a robot and not the firing robot, check if it's hit
                if (((Robot) obstacle) != target) {
                    if (((Robot) obstacle).isRobotInPath(robotPosition, rangePosition)) {
                        obstaclesInFire.add(obstacle);
                    }
                }
            }
            if (obstacle instanceof IObstacle) {
                // If the obstacle is a general obstacle, check if it's hit
                if (((IObstacle) obstacle).isPathBlocked(robotPosition, rangePosition)) {
                    obstaclesInFire.add(obstacle);
                }
            }
        }

        return obstaclesInFire;
    }

    // Method to find the closest object to the firing robot
    @JsonIgnore
    public IWorldObject getClosestObject(Robot target, List<IWorldObject> obstaclesInFire) {
        // Find out how to get the closest object to the robot

        Position robotPosition = target.getRobotPosition();
        Position movingShot = new Position(robotPosition.getX(), robotPosition.getY());

        // Iterate through positions to find the closest object
        for (int i = 1; i <= target.getDamagePerShot() ; i++) {
            for (IWorldObject object : obstaclesInFire) {
                switch (target.getDirection()) {
                    case "NORTH":
                        movingShot = new Position(robotPosition.getX(), robotPosition.getY() + i);
                        break;
                    case "SOUTH":
                        movingShot = new Position(robotPosition.getX(), robotPosition.getY() - i);
                        break;
                    case "EAST":
                        movingShot = new Position(robotPosition.getX() + i, robotPosition.getY());
                        break;
                    case "WEST":
                        movingShot = new Position(robotPosition.getX() - i, robotPosition.getY());
                        break;
                }

                // Check if the object is hit by the shot
                if (object instanceof SquareObstacle) {
                    if (((SquareObstacle) object).isPathBlocked(robotPosition, movingShot)) {
                        return object;
                    }
                }

                if (object instanceof Robot && object != target) {
                    if (((Robot) object).isRobotInPath(robotPosition, movingShot)) {
                        return object;
                    }
                }
            }
        }

        return null;
    }


    // Method to calculate the distance to the hit object
    @JsonIgnore
    public int getDistanceToObject(Robot target, IWorldObject object) {
        // Calculate distances based on the direction of the firing robot

        int objectY = 0;
        int objectX = 0;
        int robotY = target.getRobotPosition().getY();
        int robotX = target.getRobotPosition().getX();

        // Ensure non-negative values for distance calculation
        if (robotX <= 0) {
            robotX *= -1;
        }

        if (robotY <= 0){
            robotY *= -1;
        }

        // Calculate distances based on the direction
        switch (target.getDirection()) {
            case "NORTH":
                if (object.getTypeOfObject() == TypeOfObject.ROBOT) {
                    objectY = ((Robot) object).getRobotPosition().getY();
                } else if (object.getTypeOfObject() == TypeOfObject.OBSTACLE) {
                    objectY = ((SquareObstacle) object).getBottomLeftY();
                }
                // Ensure non-negative values for distance calculation
                if (objectY < 0){
                    objectY *= -1;
                }
                return objectY - robotY;
            case "SOUTH":
                if (object.getTypeOfObject() == TypeOfObject.ROBOT) {
                    objectY = ((Robot) object).getRobotPosition().getY();
                } else if (object.getTypeOfObject() == TypeOfObject.OBSTACLE) {
                    objectY = ((SquareObstacle) object).getTopLeftY();
                }
                // Ensure non-negative values for distance calculation
                if (objectY < 0){
                    objectY *= -1;
                }
                return robotY - objectY;
            case "EAST":
                if (object.getTypeOfObject() == TypeOfObject.ROBOT) {
                    objectX = ((Robot) object).getRobotPosition().getX();
                } else if (object.getTypeOfObject() == TypeOfObject.OBSTACLE) {
                    objectX = ((SquareObstacle) object).getBottomLeftX();
                }
                // Ensure non-negative values for distance calculation
                if (objectX < 0){
                    objectX *= -1;
                }
                return objectX - robotX;
            case "WEST":
                if (object.getTypeOfObject() == TypeOfObject.ROBOT) {
                    objectX = ((Robot) object).getRobotPosition().getX();
                } else if (object.getTypeOfObject() == TypeOfObject.OBSTACLE) {
                    objectX = ((SquareObstacle) object).getBottomRightX();
                }
                // Ensure non-negative values for distance calculation
                if (objectX < 0){
                    objectX *= -1;
                }
                return robotX - objectX;
        }

        return 0;
    }

    // Method to calculate the position at the maximum firing range
    @JsonIgnore
    public Position getMaxRangePosition(String direction, Position robotPosition, int fireRange){
        // Calculate the position at the maximum firing range in the specified direction

        Position rangePosition = new Position(0,0);

        switch (direction) {
            case "NORTH":
                rangePosition = new Position(robotPosition.getX(),
                        robotPosition.getY() + fireRange);
                break;
            case "SOUTH":
                rangePosition = new Position(robotPosition.getX(),
                        robotPosition.getY() - fireRange);
                break;
            case "EAST":
                rangePosition = new Position(robotPosition.getX() + fireRange,
                        robotPosition.getY());
                break;
            case "WEST":
                rangePosition = new Position(robotPosition.getX() - fireRange,
                        robotPosition.getY());
                break;
        }
        return rangePosition;
    }

}

