package de.wethinkco.robotworlds.protocol;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.Map;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponseMessage extends ResponseMessage{
    public ErrorResponseMessage(){}
    public ErrorResponseMessage(String result, Map<String, Object> data) {
        super(result, data);
    }
}
