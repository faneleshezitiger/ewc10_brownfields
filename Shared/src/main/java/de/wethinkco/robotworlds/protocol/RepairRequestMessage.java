package de.wethinkco.robotworlds.protocol;

import de.wethinkco.robotworlds.protocol.robots.Robot;
import de.wethinkco.robotworlds.protocol.world.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepairRequestMessage extends RequestMessage {

    public RepairRequestMessage() {
        super("", "repair", new ArrayList<>());
    }

    public RepairRequestMessage(String robot, List<Object> arguments) {
        super(robot, "repair", arguments);
    }

    public RepairRequestMessage(String robot) {
        super(robot, "repair", new ArrayList<>());
    }

    @Override
    public ResponseMessage execute(Robot target, World world) {
        if (target.isDead()) {
            Map<String, Object> mapData = new HashMap<>();
            mapData.put("message", "Cannot repair a dead robot.");
            return new ErrorResponseMessage("ERROR", mapData);
        }

        // Set the robot's shield to the maximum value (you can adjust this value as needed).
        target.setShields(10);

        // Placeholder response for now.
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("message", "Repaired");
        return new SuccessResponseMessage("OK", mapData, target);
    }

}