package de.wethinkco.robotworlds.protocol;

import de.wethinkco.robotworlds.protocol.robots.Robot;
import de.wethinkco.robotworlds.protocol.world.World;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ErrorRequestMessage extends RequestMessage{

    public ErrorRequestMessage(String robot, List<Object> args){
        super(robot, "error", args);
    }

    public ErrorRequestMessage(){

    }

    @Override
    public ResponseMessage execute(Robot target, World world) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("message", "invalid command provided");
        return new ErrorResponseMessage("ERROR", mapData);
    }
}