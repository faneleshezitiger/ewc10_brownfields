package de.wethinkco.robotworlds.protocol.world;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class WorldProperties {

    private static final Scanner scan = new Scanner(System.in);

    public static int getHeight(){
        while(true){
            try{
                System.out.println("Enter height: ");
                String ans = scan.nextLine();
                if(Integer.parseInt(ans)>0){
                    return Integer.parseInt(ans);
                }else{
                    System.out.println("Height has to be a positive integer.");
                }

            }catch (NumberFormatException e){
                System.out.println("Height has to be a positive integer.");
            }
        }
    }

    public static int getWidth(){
        while(true){
            try{
                System.out.println("Enter Width: ");
                String ans = scan.nextLine();
                if(Integer.parseInt(ans)>0){
                    return Integer.parseInt(ans);
                }else{
                    System.out.println("Width has to be a positive integer.");
                }

            }catch (NumberFormatException e){
                System.out.println("Width has to be a positive integer.");
            }
        }
    }

    public static int getVisibility(){
        while(true){
            try{
                System.out.println("Enter visibility: ");
                String ans = scan.nextLine();
                if(Integer.parseInt(ans)>0){
                    return Integer.parseInt(ans);
                }else{
                    System.out.println("Visibility has to be a positive integer.");
                }

            }catch (NumberFormatException e){
                System.out.println("Visibility has to be a positive integer.");
            }
        }
    }

    private static JsonNode getJSONInfo(ObjectMapper mapper) throws IOException {
        String projectRootPath = System.getProperty("user.dir");
        String configFilePath = projectRootPath.concat("/Configuration.json");

        return mapper.readTree(new File(configFilePath));
    }


    public static void changeProperties(int width, int height, int visibility){

        try {
            String projectRootPath = System.getProperty("user.dir");
            String configFilePath = projectRootPath.concat("/Configuration.json");

            ObjectMapper objectMapper = new ObjectMapper();

            JsonNode rootNode =getJSONInfo(objectMapper);

            // Access specific fields in the JSON
            ObjectNode gridSizeNode =(ObjectNode) rootNode.path("gridSize");
            gridSizeNode.put("height",height);
            gridSizeNode.put("width",width);

            ObjectNode robotVisionFieldNode =(ObjectNode) rootNode.path("robotVisionField");
            robotVisionFieldNode.put("visibility", visibility);

            // Print the values
            objectMapper.writeValue(new File(configFilePath), rootNode);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void showProperties() throws IOException {
        JsonNode rootNode =getJSONInfo(new ObjectMapper());
        // Access specific fields in the JSON
        ObjectNode gridSizeNode =(ObjectNode) rootNode.path("gridSize");
        ObjectNode robotVisionFieldNode =(ObjectNode) rootNode.path("robotVisionField");
        int height = gridSizeNode.path("height").asInt();
        int width = gridSizeNode.path("width").asInt();
        int visibility = robotVisionFieldNode.path("visibility").asInt();
        System.out.println("Current World Properties: ");
        System.out.println("Height: " + height + "\nWidth: " + width);
        System.out.println("Visibility: " + visibility);
    }


}
